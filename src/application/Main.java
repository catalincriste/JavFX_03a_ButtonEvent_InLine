package application;
	
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

import javafx.scene.layout.StackPane;
//Eventos em JavaFX - pelo m�todo das classes an�nimas Inline
/*setOnAction(A��o) � um m�todo que apanha um qualquer evento
* As a��es escrevem-se no m�todo handle(), da classe EventHandler
* Duas solu��es para este m�todo :
* Sol2   - Aqui n�o se implementa a Interface, mas cria-se um objeto an�nimo do mesmo tipo no  
* parametro de entrada do pr�prio m�todo que ativa o Listener : setOn
* 
* 
* Notas Finais : Inline significa declarar um objeto de uma classe anonima como argumento (new Class)
* 				  An�nimo porque n�o tem a parte declarativa OBJETO obj = new Objeto , mas apenas
* 				  new Objeto. Esta pr�tica s� pode ser usada num parametro de um m�todo
* 				  H� varios tipos de eventos : MouseEvent,KeyboardEvent, ActionEvent, DragEvent */
public class Main extends Application {
	Button btn;									//Cria Bot�o null, fora dos m�todos para ser acess�vel a todos
	@Override
	public void start(Stage primaryStage) {
		try {
			btn = new Button("Clica-me");  //btn � agora um bot�o inicializado
			btn.setOnAction(new EventHandler<ActionEvent>(){//setOnAction Ativa a captura de eventos do tipo click e
															//This Informa que deve procurar o m�todo handler() algures dentro desta classe
				//A a��o � tratado pelo objetos an�nimo , INLINE(dentro do parametro)
				// -  Objeto An�nimo (sem nome) da classe EventHandler<evento a capturar>
				// - Inline: dentro de parenteses faz-se o override ao seu m�todo handle()
				@Override
				public void handle(ActionEvent event) {
					// TODO Auto-generated method stub
					if(event.getSource() == btn){
						System.out.println("Fui Clicado");
					}
				}

			});	
			
			
										
			StackPane layout = new StackPane();		//Layout do tipo StackPane
			layout.getChildren().add(btn);		 //adiciona o bot�o ao Layout
			Scene scene = new Scene(layout,600,400);	 //Cria a scene com o layout incluido
			
			primaryStage.setTitle("Tutorial 2 - Eventos");
			primaryStage.setScene(scene);		 //Define a janela (stage)) como a scene incluida
			primaryStage.show();				//Executa a janela
			
			
		} catch(Exception e) {					//Tratamento Gen�rico das Exe��es
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}

